from django.db import models
from datetime import date
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now

@receiver(post_save, sender=User)
def create_profile_for_new_user(sender, instance, created, **kwargs):
    print("Crea UserProfile")
    if created:
        profile=UserProfile(user=instance)
        profile.save()
class UserProfile(models.Model):
    #username = models.CharField(max_length=20, primary_key=True)
    user = models.OneToOneField(User,on_delete=models.CASCADE)

    karma = models.IntegerField(default=1)
    created = models.DateField(default= date.today)
    about = models.CharField(max_length=200)

    def __str__(self):
        return self.user.username

class Contribution(models.Model):
    title = models.CharField(max_length=200)
    date = models.DateTimeField(default=now)
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True)
    likes = models.ManyToManyField(UserProfile, related_name = 'blog_posts')
    ncomments = models.IntegerField(default=0)

    def total_likes(self):
        return self.likes.count()
    likes_ordenar = property(total_likes)

    def __str__(self):
        return self.title


class Contribution_points(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    contribution = models.ForeignKey(Contribution, on_delete=models.CASCADE)

    def __str(self):
        return self.user

class Url(models.Model):
    url = models.CharField(max_length=200, primary_key=True)
    contribution = models.ForeignKey(Contribution, on_delete=models.CASCADE, related_name="url")

    def __str__(self):
        return self.url

class Ask(models.Model):
    text = models.CharField(max_length=200)
    contribution = models.ForeignKey(Contribution, on_delete=models.CASCADE, related_name="ask")

    def __str__(self):
        return self.text

class OnlyTitle(models.Model):
    contribution = models.ForeignKey(Contribution, on_delete=models.CASCADE, related_name="ot")

    def __str__(self):
        return self.contribution.title

class Commentary(models.Model):
    text = models.CharField(max_length=200)
    created = models.DateTimeField(default=now)
    contribution = models.ForeignKey(Contribution, on_delete=models.CASCADE, related_name = "comments")
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    father_comm = models.ForeignKey('self', null=True, blank=True, related_name='replies', on_delete=models.CASCADE)
    likes = models.ManyToManyField(UserProfile, related_name = 'blog_posts2')

    def total_likes(self):
        return self.likes.count()

    def __str__(self):
        return self.text

class Commentary_points(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    commentary = models.ForeignKey(Commentary, on_delete=models.CASCADE)

    def __str(self):
        return self.user
