from django import forms

from .models import Commentary, Contribution

class CommentForm(forms.ModelForm):

    class Meta:
        model = Commentary
        fields = ('text',)


class SubmitForm(forms.Form):
    title = forms.CharField()
    url = forms.CharField()
    text = forms.CharField()
