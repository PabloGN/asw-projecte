from django.contrib import admin
from django.urls import path
from . import views
from rest_framework.routers import SimpleRouter


router = SimpleRouter()

router.register(r'profile', views.ProfileViewSet, 'Profile')
router.register(r'profileupdate', views.ProfileUpdateViewSet, 'ProfileUpdate')
router.register(r'upvotedCommentaries', views.UpvotedCommentariesViewSet, 'Upvoted commentaries')
router.register(r'upvotedSubmissions', views.UpvotedSubmissionsViewSet, 'Upvoted submissions')
router.register(r'new', views.NewSet, "New")
router.register(r'ask', views.AskSet, "Ask")
router.register(r'home', views.HomeSet, "Home")
router.register(r'ContributionDetail', views.ContributionDetalViewSet, "contribution_detail")
router.register(r'ContributionComments', views.ContributionCommentsSet, "contribution_comments")
router.register(r'submit', views.SubmitSet, 'Submit')
router.register(r'threads', views.ThreadsSet, 'Threads')
router.register(r'AddComment', views.AddCommentSet, 'AddComment')
router.register(r'LikeContribution', views.LikeContributionSet, 'LikeContribution')
router.register(r'UnlikeContribution', views.UnlikeContributionSet, 'UnlikeContribution')
router.register(r'LikeCommentary', views.LikeCommentarySet, 'LikeCommentary')
router.register(r'UnlikeCommentary', views.UnlikeCommentarySet, 'UnlikeCommentary')
router.register(r'Submissions', views.SubmissionsViewSet, 'Submissions')


router.register(r'Reply', views.ReplyCommentSet, 'Reply')




urlpatterns = [
    path('', views.home, name='Home'),
    path('home', views.home, name='Home'),
    path('submit', views.submit, name='Submit'),
    path('new', views.new, name='New'),
    path(r'threads?id=<int:pk>', views.threads, name='Threads'),
    path('ask', views.ask, name = 'Ask'),
    path(r'profile?id=<int:pk>', views.profile, name = 'Profile'),
    path(r'submissions?id=<int:pk>', views.submissions, name='Submissions'),
    path(r'upvotedcomments?id=<int:pk>', views.upvotedc, name='Upvotedc'),
    path(r'upvotedsubmissions?id=<int:pk>', views.upvoteds, name='Upvoteds'),
    path('contribution/<int:pk>/', views.contribution_detail, name='post_detail'),
    path('cont_comment/(?P<pk>[0-9]+)/comment/$''', views.add_comment_to_post, name='add_comment_to_post'),
    path('like/<int:pk>', views.like_post, name='like_post'),
    path('unlike/<int:pk>', views.unlike_post, name='unlike_post'),
    path('like/<int:pk>/<int:pk_comment>', views.like_comment, name='like_comment'),
    path('unlike/<int:pk>/<int:pk_comment>', views.unlike_comment, name='unlike_comment'),
    path('reply_comment/(?P<pk>[0-9]+)/reply/<int:pk_comment>$''', views.reply_comment, name='reply_comment'),
    path('like_h/<int:pk>', views.like_post_home, name='like_post_home'),
    path('unlike_h/<int:pk>', views.unlike_post_home, name='unlike_post_home'),
    path('like_n/<int:pk>', views.like_post_new, name='like_post_new'),
    path('unlike_n/<int:pk>', views.unlike_post_new, name='unlike_post_new'),
    path('like_a/<int:pk>', views.like_post_ask, name='like_post_ask'),
    path('unlike_a/<int:pk>', views.unlike_post_ask, name='unlike_post_ask'),
    path('like_comm_th/<int:pk>/<int:pk_comment>', views.like_comment_threads, name='like_comm_threads'),
    path('unlike_comm_th/<int:pk>/<int:pk_comment>', views.unlike_comment_threads, name='unlike_comm_threads'),
    #path('url/<int:pk>', views.url_post, name='url_post')
]

urlpatterns += router.urls
