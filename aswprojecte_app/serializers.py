from rest_framework.serializers import ModelSerializer
from .models import (User, UserProfile, Contribution, Contribution_points, Url, Ask, Commentary, Commentary_points, OnlyTitle)
from rest_framework import serializers

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')

class UserProfileIdSerializer(ModelSerializer):
    user=UserSerializer(read_only=True)
    class Meta:
        model = UserProfile
        fields = ('id', 'user')

class UserProfileSerializer(ModelSerializer):

    user =UserSerializer(read_only = True)
    class Meta:
        model = UserProfile
        fields = ('id', 'karma', 'created', 'about', 'user' )

class ContributionSerializer(ModelSerializer):
    user=UserProfileIdSerializer(read_only=True)
    url=serializers.SlugRelatedField(slug_field='url', read_only=True, allow_null=True, many=True)
    ask=serializers.SlugRelatedField(slug_field='text', read_only=True, allow_null=True, many=True)

    class Meta:
        model = Contribution
        fields = ('id', 'title', 'date', 'user', 'likes', 'ncomments', 'url', 'ask')

class ContributiononlytitleSerializer(ModelSerializer):

    class Meta:
        model = Contribution
        fields = ('id', 'title')

class Contribution_pointsSerializer(ModelSerializer):
    class Meta:
        model = Contribution_points
        fields = '__all__'

class UrlSerializer(ModelSerializer):
    contribution = ContributionSerializer(read_only=True)
    class Meta:
        model = Url
        fields = '__all__'

class AskSerializer(ModelSerializer):
    contribution = ContributionSerializer(read_only=True)
    class Meta:
        model = Ask
        fields = '__all__'

class OnlyTitleSerializer(ModelSerializer):
    class Meta:
        model = OnlyTitle
        fields = '__all__'

class RecursiveSerializer(serializers.Serializer):
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data

class CommentaryWithoutRepliesSerializer(ModelSerializer):
    user = UserProfileIdSerializer(read_only=True)
    contribution = ContributiononlytitleSerializer(read_only=True)
    class Meta:
        model = Commentary
        fields = ('text', 'created', 'contribution', 'user', 'father_comm', 'likes','id')

class CommentarySerializer(ModelSerializer):
    replies = RecursiveSerializer(many=True, read_only=True)
    class Meta:
        model = Commentary
        fields = ('text', 'created', 'contribution', 'user', 'father_comm', 'likes', 'replies','id')

class Commentary_pointsSerializer(ModelSerializer):
    class Meta:
        model = Commentary_points
        fields = '__all__'

class ContributionDetailSerializer(ModelSerializer):
    user=UserProfileIdSerializer(read_only=True)
    commentas = CommentarySerializer(many=True, read_only=True)
    url=serializers.SlugRelatedField(slug_field='url', read_only=True, allow_null=True, many=True)
    ask=serializers.SlugRelatedField(slug_field='text', read_only=True, allow_null=True, many=True)
	
    class Meta:
        model = Contribution
        fields = ('id', 'title', 'url', 'ask', 'date', 'user', 'likes', 'ncomments', 'commentas')