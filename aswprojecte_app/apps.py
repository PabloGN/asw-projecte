from django.apps import AppConfig


class AswprojecteAppConfig(AppConfig):
    name = 'aswprojecte_app'
