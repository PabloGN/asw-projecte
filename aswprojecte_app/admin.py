from django.contrib import admin
from .models import UserProfile

from .models import *

admin.site.register(Contribution)
admin.site.register(UserProfile)
admin.site.register(Commentary_points)
admin.site.register(Contribution_points)
admin.site.register(Commentary)
admin.site.register(Ask)
admin.site.register(Url)
admin.site.register(OnlyTitle)

# Register your models here.

