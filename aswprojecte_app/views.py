from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import logout
from django.shortcuts import redirect
from .forms import CommentForm
from django.shortcuts import render, get_object_or_404
from django import forms
from .forms import SubmitForm
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from .models import (Contribution, UserProfile, Url, Ask, Commentary_points, Commentary, Contribution_points, User, OnlyTitle)
from .serializers import (ContributionSerializer, UserProfileSerializer, UrlSerializer, AskSerializer, Commentary_pointsSerializer, CommentarySerializer, CommentaryWithoutRepliesSerializer, Contribution_pointsSerializer, UserSerializer, OnlyTitleSerializer, ContributionDetailSerializer)
from django.db.models import Case, When
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from drf_yasg import openapi
from allauth.socialaccount import providers
from allauth.socialaccount.models import SocialLogin, SocialToken, SocialApp, SocialAccount
from django.shortcuts import get_object_or_404
from allauth.socialaccount.providers.facebook.views import fb_complete_login
from allauth.socialaccount.helpers import complete_social_login
import allauth.account
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError



class ProfileViewSet(ViewSet):

    @swagger_auto_schema(operation_description='Obtiene el perfil del usuario con el id que se le pasa como parametro', security=[], responses= {404: 'No profile with that Id', 200: UserProfileSerializer})
    def retrieve(self, request, pk=None):
        usr= UserProfile.objects.filter(pk=pk).count()
        if usr > 0:
            queryset = UserProfile.objects.all()
            item = get_object_or_404(queryset, pk=pk)
            serializer = UserProfileSerializer(item)
            return Response(serializer.data)
        else:
            return Response('No profile with that id', status=404)
class ProfileUpdateViewSet(ViewSet):

    @swagger_auto_schema(operation_description='Modifica el campo about del perfil del usuario con el id que se pasa por parámetro', responses= {404: 'No profile with that Id', 403: 'Your api key is not valid', 200: UserProfileSerializer}, request_body = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'about': openapi.Schema(type=openapi.TYPE_STRING, description='Proporciona el about para actualizar el perfil', example= 'Soy una descripción de ejemplo'),
    }
    ))
    def update(self, request, pk=None):
        if not 'HTTP_AUTHORIZATION' in request.META:
            return Response('Unauthorized', status=401)
        else:
            u= UserProfile.objects.filter(pk=pk).count()
            if u > 0:
                if SocialAccount.objects.filter(uid=request.META['HTTP_AUTHORIZATION']).exists():
                    print("Existe ese user")
                    social = SocialAccount.objects.get(uid=request.META['HTTP_AUTHORIZATION'])
                    usr = User.objects.get(socialaccount=social)
                    usrprof = UserProfile.objects.get(user=usr)
                    if pk == str(usr.id):
                        queryset = UserProfile.objects.get(user=usr.pk)
                        serializer = UserProfileSerializer(queryset, data=request.data)
                        if serializer.is_valid():
                            serializer.save()
                            return Response(serializer.data, status=200)
                else:
                    return Response('Your api key is not valid', status=403)
            else:
                return Response('No profile with that id', status=404)       
        return Response('Bad request', status=400)


class SubmissionsViewSet(ViewSet):

    @swagger_auto_schema(operation_description='Obtiene el perfil del usuario con el id que se le pasa como parametro', security=[], responses= {404: 'No profile with that Id', 401: 'You provided no api key', 200: ContributionSerializer})
    def retrieve(self, request, pk=None):
        usr= UserProfile.objects.filter(pk=pk).count()
        if usr > 0:
            usr = UserProfile.objects.get(pk=pk)
            queryset = Contribution.objects.filter(user = usr).order_by('-date')
            serializer = ContributionSerializer(queryset, many=True)
            return Response(serializer.data)
        else:
            return Response('No profile with that id', status=404)

class UpvotedCommentariesViewSet(ViewSet):

    @swagger_auto_schema(operation_description='Obtiene una lista de comentarios votados por el usuario autorizado', responses= {401: 'You provided no api key', 200: CommentaryWithoutRepliesSerializer})
    def retrieve(self, request, pk=None):
        if not 'HTTP_AUTHORIZATION' in request.META:
            return Response('Unauthorized', status=401)
        else:
            #social = SocialAccount.objects.get(uid=request.META['HTTP_AUTHORIZATION'])
            #usr = User.objects.get(socialaccount=social)
            #up = UserProfile.objects.get(user=usr.pk)
            cp = Commentary_points.objects.filter(user=pk).values('commentary')
            queryset = Commentary.objects.filter(pk__in = cp)
            serializer = CommentaryWithoutRepliesSerializer(queryset, many=True)
            return Response(serializer.data, status=200)
        return Response('Bad request', status=400)

class UpvotedSubmissionsViewSet(ViewSet):

    @swagger_auto_schema(operation_description='Obtiene una lista de submissions votadas por el usuario autorizado', responses= {401: 'You provided no api key', 200: ContributionSerializer})
    def retrieve(self, request, pk=None):
        if not 'HTTP_AUTHORIZATION' in request.META:
            return Response('Unauthorized', status=401)
        else:
            #social = SocialAccount.objects.get(uid=request.META['HTTP_AUTHORIZATION'])
            #usr = User.objects.get(socialaccount=social)
            #up = UserProfile.objects.get(user=usr.pk)
            sp = Contribution_points.objects.filter(user=pk).values('contribution')
            queryset = Contribution.objects.filter(pk__in=sp)
            serializer = ContributionSerializer(queryset, many=True)
            return Response(serializer.data)

class HomeSet(ViewSet):

    def list(self, request):
        queryset = sorted(Contribution.objects.exclude(ask__isnull=False),  key=lambda m: -m.likes_ordenar)
        serializer = ContributionSerializer(queryset, many=True)
        return Response(serializer.data)

class NewSet(ViewSet):

    @swagger_auto_schema(operation_description='Obtiene todas las contribuciones ordenadas por momento de creacion descendiente',
                        security=[]
    )
    def list(self, request):
        queryset = Contribution.objects.order_by('-date')

        serializer = ContributionSerializer(queryset, many=True)
        return Response(serializer.data)

class AskSet(ViewSet):

    @swagger_auto_schema(operation_description='Obtiene todas las contribuciones de tipo ask por momento de creacion descendiente',
                        security=[]
    )
    def list(self, request):
        queryset = Contribution.objects.order_by('-date').exclude(url__isnull=False) & Contribution.objects.order_by('-date').exclude(ot__isnull=False)
        serializer = ContributionSerializer(queryset, many=True)
        return Response(serializer.data)

class ContributionDetalViewSet(ViewSet):

    @swagger_auto_schema(operation_description='Obtiene una presentación detallada de una contribución concreta', responses= {404: 'contribution not found', 200: ContributionSerializer})
    def retrieve(self, request, pk=None):
        post = get_object_or_404(Contribution.objects.all(), pk=pk)
        comms = Commentary.objects.filter(contribution=post)
        post.commentas = comms.filter(father_comm__isnull=True)
        serializer = ContributionDetailSerializer(post)
        return Response(serializer.data)

class LikeContributionSet(ViewSet):
    @swagger_auto_schema(operation_description='Like Contribution', responses= {401: 'You provided no api key', 401: 'You provided no api key', 404: 'contribution not found', 200: ContributionSerializer, 402: "Already liked!"})
    def update(self, request, pk=None):
        if not 'HTTP_AUTHORIZATION' in request.META:
            return Response('Unauthorized', status=401)
        else:
            if SocialAccount.objects.filter(uid=request.META['HTTP_AUTHORIZATION']).exists():
                social = SocialAccount.objects.get(uid=request.META['HTTP_AUTHORIZATION'])
                usr = User.objects.get(socialaccount=social)
                user = UserProfile.objects.get(user=usr)
                post =  get_object_or_404(Contribution,pk=pk)
                if(Contribution_points.objects.filter(user=user,contribution=post).exists()):
                    return Response('Already liked', status = 402)
                else:
                    user = UserProfile.objects.get(user=usr)
                    new_contribution_point = Contribution_points(user = user, contribution = post)
                    new_contribution_point.save()
                    post.likes.add(request.user.id)
                    post.save()
                    serializer = ContributionSerializer(post)
                    return Response(serializer.data)
            else:
                return Response('Unauthorized', status=401)

class UnlikeContributionSet(ViewSet):
    @swagger_auto_schema(operation_description='Unlike Contribution', responses= {401: 'You provided no api key', 404: 'contribution not found', 200: ContributionSerializer, 402: "It is not liked!"})
    def update(self, request, pk=None):
        if not 'HTTP_AUTHORIZATION' in request.META:
            return Response('Unauthorized', status=401)
        else:
            if SocialAccount.objects.filter(uid=request.META['HTTP_AUTHORIZATION']).exists():
                social = SocialAccount.objects.get(uid=request.META['HTTP_AUTHORIZATION'])
                usr = User.objects.get(socialaccount=social)
                user = UserProfile.objects.get(user=usr)
                post =  get_object_or_404(Contribution, pk=pk)
                if(Contribution_points.objects.filter(user=user,contribution=post).exists()):
                    del_contribution_point = Contribution_points.objects.get(user = user, contribution = post)
                    del_contribution_point.delete()
                    post.likes.remove(request.user.id)
                    post.save()
                    serializer = ContributionSerializer(post)
                    return Response(serializer.data)
                else:
                    return Response('It is not liked!', status = 402)
            else:
                return Response('Unauthorized', status=401)


class LikeCommentarySet(ViewSet):
    @swagger_auto_schema(operation_description='Like Commentary', responses= {404: 'Commentary not found', 200: CommentarySerializer, 402: "Already liked!", 401: 'You provided no api key'})
    def update(self, request, pk=None):
        if not 'HTTP_AUTHORIZATION' in request.META:
            return Response('Unauthorized', status=401)
        else:
            if SocialAccount.objects.filter(uid=request.META['HTTP_AUTHORIZATION']).exists():
                social = SocialAccount.objects.get(uid=request.META['HTTP_AUTHORIZATION'])
                usr = User.objects.get(socialaccount=social)
                user = UserProfile.objects.get(user=usr)
                comm = get_object_or_404(Commentary,pk=pk)
                if(Commentary_points.objects.filter(user=user,commentary=comm).exists()):
                    return Response('Already liked', status = 402)
                else:
                    new_comm_point = Commentary_points(user = user, commentary = comm)
                    new_comm_point.save()
                    comm.likes.add(request.user.id)
                    comm.save()
                    serializer = CommentarySerializer(comm)
                    return Response(serializer.data)
            else:
                return Response('Unauthorized', status=401)

class UnlikeCommentarySet(ViewSet):
    @swagger_auto_schema(operation_description='Like Commentary', responses= {401: 'You provided no api key', 404: 'commentary not found', 200: CommentarySerializer, 402: "It is not liked!"})
    def update(self, request, pk=None):
        if not 'HTTP_AUTHORIZATION' in request.META:
            return Response('Unauthorized', status=401)
        else:
            if SocialAccount.objects.filter(uid=request.META['HTTP_AUTHORIZATION']).exists():
                social = SocialAccount.objects.get(uid=request.META['HTTP_AUTHORIZATION'])
                usr = User.objects.get(socialaccount=social)
                user = UserProfile.objects.get(user=usr)
                comm = get_object_or_404(Commentary,pk=pk)
                if(Commentary_points.objects.filter(user=user,commentary=comm).exists()):
                    del_comm_point = Commentary_points.objects.get(user = user, commentary = comm)
                    del_comm_point.delete()
                    comm.likes.remove(request.user.id)
                    comm.save()
                    serializer = CommentarySerializer(comm)
                    return Response(serializer.data)
                else:
                    return Response('It is not liked!', status = 402)
            else:
                return Response('Unauthorized', status=401)

class ContributionCommentsSet(ViewSet):
    @swagger_auto_schema(operation_description='Obtiene los comentarios de una contribución concreta', responses= {404: 'contribution not found', 200: CommentarySerializer})
    def retrieve(self, request, pk=None):
        post = get_object_or_404(Contribution,pk=pk)
        queryset = Commentary.objects.filter(contribution_id=pk)
        serializer = CommentarySerializer(queryset, many=True)
        return Response(serializer.data)

class AddCommentSet(ViewSet):
    @swagger_auto_schema(
        operation_description='Da de alta un nuevo comentario.\n Es obligatorio introducir un texto.\n  Es obligatorio introducir el identificador de la contribucion a la que se desea comentar.',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            description='Proporciona un title i els camps deseats',
            properties= {
                'text': openapi.Schema(type=openapi.TYPE_STRING,
                                    description='Proporciona el texto del comentario',
                                    example= 'Soy un ejemplo de texto'),
                'id_contribution': openapi.Schema(type=openapi.TYPE_STRING,
                                    description='Proporciona elidentificador de la contribucion',
                                    example= '1'),
            },
            required=["text", "id"],
        ),responses= {404: 'contribution to comment not found', 200: CommentarySerializer, 401: 'You provided no api key'})
    def create(self, request):
        if not 'HTTP_AUTHORIZATION' in request.META:
            return Response('Unauthorized', status=401)
        else:
            if SocialAccount.objects.filter(uid=request.META['HTTP_AUTHORIZATION']).exists():
                social = SocialAccount.objects.get(uid=request.META['HTTP_AUTHORIZATION'])
                usr = User.objects.get(socialaccount=social)
                user = UserProfile.objects.get(user=usr)
                comment = Commentary(text = request.data['text'])
                #println(user.id)
                c = get_object_or_404(Contribution,pk=request.data['id_contribution'])
                comment.user = user
                comment.contribution = Contribution.objects.get(id=request.data['id_contribution'])
                comment.father_comm__isnull=False
                comment.save()
                user.karma += 1
                user.save()
                c.ncomments= c.ncomments + 1
                c.save()
                queryset = Commentary.objects.get(id=comment.id)
                serializer = CommentarySerializer(comment)
                return Response(serializer.data)
            else:
                return Response('Unauthorized', status=401)



class ReplyCommentSet(ViewSet):
    @swagger_auto_schema(
        operation_description='Da de alta una nueva reply.\n Es obligatorio introducir un texto.\n  Es obligatorio introducir el identificador del comentario al que se desea hacer reply.',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            description='Proporciona un texto y un identificador',
            properties= {
                'text': openapi.Schema(type=openapi.TYPE_STRING,
                                    description='Proporciona el texto del comentario',
                                    example= 'Soy un ejemplo de texto'),
                'id_commentary': openapi.Schema(type=openapi.TYPE_STRING,
                                    description='Proporciona el identificador del comentario a hacer reply',
                                    example= '3'),
            },
            required=["text","id_commentary"],
        ), responses= {401: 'You provided no api key', 404: 'commentary parent not found', 200: CommentarySerializer})
    def create(self, request):
        if not 'HTTP_AUTHORIZATION' in request.META:
            return Response('Unauthorized', status=401)
        else:
            if SocialAccount.objects.filter(uid=request.META['HTTP_AUTHORIZATION']).exists():
                social = SocialAccount.objects.get(uid=request.META['HTTP_AUTHORIZATION'])
                usr = User.objects.get(socialaccount=social)
                user = UserProfile.objects.get(user=usr)
                reply = Commentary(text=request.data['text'])
                comment_parent = get_object_or_404(Commentary, pk=request.data['id_commentary'])
                user.karma += 1
                user.save()
                reply.user = user
                reply.contribution = get_object_or_404(Contribution, pk=comment_parent.contribution_id)
                reply.father_comm = comment_parent
                reply.father_comm__isnull=False
                reply.save()
                post = get_object_or_404(Contribution, id=reply.contribution_id)
                post.ncomments= post.ncomments + 1
                post.save()
                queryset = get_object_or_404(Commentary,id=reply.id)
                serializer = CommentarySerializer(reply)
                return Response(serializer.data)
            else:
                return Response('Unauthorized', status=401)

class SubmitSet(ViewSet):

    @swagger_auto_schema(
        operation_description='Da de alta una nueva Contribucion.\n Es obligatorio introducir un titulo.\n La url introducida ha de ser valida.\n Si se introducen todos los campos, el texto pasa a ser un comentario.',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            description='Proporciona un title i els camps deseats',
            properties= {
                'title': openapi.Schema(type=openapi.TYPE_STRING,
                                    description='Proporciona el titulo de la contribucion',
                                    example= 'Soy un titulo de ejemplo'),
                'url': openapi.Schema(type=openapi.TYPE_STRING,
                                    description='Proporciona una url para la contribucion',
                                    example='https://www.google.com/'),
                'text': openapi.Schema(type=openapi.TYPE_STRING,
                                    description='Proporciona una texto para la contribucion',
                                    example='Soy un texto de ejemplo'),
            },
            required=["title"],
        ),
        responses={201: ContributionSerializer, 400: "Something went wrong", 302: ContributionSerializer, 401: "Unauthorized"},
    )
    def create(self, request):
        if not 'HTTP_AUTHORIZATION' in request.META:
            return Response('Unauthorized', status=401)
        else:
            if SocialAccount.objects.filter(uid=request.META['HTTP_AUTHORIZATION']).exists():
                print("Existe ese user")
                social = SocialAccount.objects.get(uid=request.META['HTTP_AUTHORIZATION'])
                usr = User.objects.get(socialaccount=social)
                usrprof = UserProfile.objects.get(user=usr)
            else:
                return Response('Unauthorized', status=401)
                print("no existe")
        if not 'title' in request.data or not request.data['title']:
            return Response('Field title is missing', status=400)
        #Crear contribution
        contr = Contribution(title = request.data['title'], user = usrprof)
        contr.save()
        if 'url' in request.data:
            #request de tipus url
            #check url
            if request.data['url']:
                validate = URLValidator()
                try:
                    validate(request.data['url'])
                except ValidationError:
                    return Response('The url is not valid', status=400)
                if Url.objects.filter(url=request.data['url']).exists():
                        dup = Url.objects.get(url=request.data['url'])
                        contr.delete()
                        serializer = UrlSerializer(dup)
                        return Response(serializer.data, status=302)
                #crear url contribution
                else:
                    url_contr = Url(url = request.data['url'], contribution = contr)
                    url_contr.save()
                    serializer = UrlSerializer(url_contr)
                if 'text' in request.data:
                    if request.data['text']:
                        #crear Commentary
                        comm = Commentary(text = request.data['text'], contribution = contr, user = usrprof)
                        comm.save()
                        contr.ncomments += 1
                        contr.save()
        else:
            if 'text' in request.data:
                if request.data['text']:
                    #contribucio de tipus ask
                    ask = request.data['text']
                    ask_contr = Ask(text = ask, contribution = contr)
                    ask_contr.save()
                    serializer = AskSerializer(ask_contr)
        if not 'url' in request.data and not 'text' in request.data:
            #contribucio de tipus onlytitle
            t = OnlyTitle(contribution= contr)
            t.save()
            serializer = OnlyTitleSerializer(t)

        return Response(serializer.data, status=200)


class ThreadsSet(ViewSet):
    @swagger_auto_schema(operation_description='Obtiene los comentarios del usuario con el id que se le pasa como parametro',
                        security=[],
    )
    def retrieve(self, request, pk=None):
        usr = UserProfile.objects.get(pk=pk)
        queryset = Commentary.objects.filter(user = usr).exclude(father_comm__user=usr) & Commentary.objects.filter(user = usr).exclude(father_comm__father_comm__user=usr)
        serializer = CommentarySerializer(queryset, many=True)
        return Response(serializer.data)

def get_date_posts(posts):
    return posts.get('date_posted')

def get_date(contribution):
    return posts.get('date')

def home(request):
    #posts = Contribution.objects.order_by("likes_ordenar")
    posts = sorted(Contribution.objects.all(),  key=lambda m: -m.likes_ordenar)
    titles = OnlyTitle.objects.all()
    urls = Url.objects.all()
    contr_con_likes = Contribution_points.objects.filter(user_id=request.user.id)
    print(contr_con_likes)
    return render(request, 'aswprojecte_app/home.html', {'posts': posts, 'urls': urls, 'titles': titles,'cont_likes':contr_con_likes})


def threads(request, pk):
    usr = UserProfile.objects.get(pk=pk)
    comm_con_likes = Commentary_points.objects.filter(user_id=request.user.id)
    comments = Commentary.objects.filter(user = usr).exclude(father_comm__user=usr) & Commentary.objects.filter(user = usr).exclude(father_comm__father_comm__user=usr)
    return render(request, 'aswprojecte_app/threads.html', {'comments': comments, 'user_mio':usr, 'comm_likes':comm_con_likes})

def ask(request):#Cambiar
    posts = Contribution.objects.order_by('-date')
    asks = Ask.objects.all()
    contr_con_likes = Contribution_points.objects.filter(user_id=request.user.id)
    return render(request, 'aswprojecte_app/ask.html', {'posts': posts, 'asks': asks,'cont_likes':contr_con_likes})

def profile(request, pk):#Cambiar
    u = User.objects.get(pk=pk)
    p = UserProfile.objects.get(pk=pk)
    if not SocialAccount.objects.filter(user=pk):
        social = "Not Defined"
    else: social = SocialAccount.objects.get(user=pk)
   # social = SocialAccount.objects.get(provider='Google')
   # z = user.social_auth.get(provider='Google').uid
    if request.method == 'GET':
        return render(request, 'aswprojecte_app/profile.html', {'p': p, 'u': u, 'social': social})
    elif request.method == 'POST':
        a = request.POST['about']
        p.about = a
        p.save()
        return render(request, 'aswprojecte_app/profile.html', {'p': p, 'u': u})

def submissions(request, pk):
    u=User.objects.get(pk=pk)
    up=UserProfile.objects.get(user=pk)
    posts=Contribution.objects.filter(user=up.id).order_by('-date')
    urls = Url.objects.all()
    asks = Ask.objects.all()
    titles = OnlyTitle.objects.all()
    return render(request, 'aswprojecte_app/submissions.html', {'posts': posts, 'u': u, 'urls': urls, 'asks': asks, 'titles': titles})

def upvotedc(request, pk):#Cambiar
    u=User.objects.get(pk=pk)
    up=UserProfile.objects.get(user=pk)
    c = Commentary.objects.filter(likes__pk=up.id).order_by('-created')
    return render(request, 'aswprojecte_app/upvotedc.html', {'c': c})

def upvoteds(request, pk):#Cambiar
    u=User.objects.get(pk=pk)
    up=UserProfile.objects.get(user=pk)
    posts = Contribution.objects.filter(likes__pk=up.id).order_by('-date')
    urls = Url.objects.all()
    asks = Ask.objects.all()
    titles = OnlyTitle.objects.all()
    return render(request, 'aswprojecte_app/upvoteds.html', {'posts': posts, 'u': u, 'urls': urls, 'asks': asks, 'titles': titles})

def submit(request):
    validate = URLValidator()
    if request.method == 'GET':
        return render(request, 'aswprojecte_app/submit.html')
    elif request.method == 'POST':
        form = SubmitForm(request.POST or None)
        if not form.data['title']:
            messages.add_message(request, messages.WARNING,"That's not a valid title")
            return render(request, 'aswprojecte_app/submit.html')
        else:
            uskarma = UserProfile.objects.get(pk=request.user.id)
            uskarma.karma += 5
            uskarma.save()
            title = form.data['title']
            contr = Contribution(title = title, user = UserProfile.objects.get(pk=request.user.id))
            contr.save()
            if not form.data['url']:
                if not form.data['text']:
                    t = OnlyTitle(contribution= contr)
                    t.save()
                    return HttpResponseRedirect('new')
                else:
                    #Ask contribution
                    ask = form.data['text']
                    ask_contr = Ask(text = ask, contribution = contr)
                    ask_contr.save()
            else:
                try:
                    validate(form.data['url'])
                except ValidationError:
                    messages.add_message(request, messages.WARNING, "That's not a valid url")
                    contr.delete()
                    return render(request, 'aswprojecte_app/submit.html')
                if not form.data['text']:
                    #url contribution without comment
                    url = form.data['url']
                    #Check that url is unique
                    if not Url.objects.filter(url=url).exists():
                        url_contr = Url(url = url, contribution = contr)
                        url_contr.save()
                    else:
                        dup = Url.objects.get(url=url)
                        contr.delete()
                        return HttpResponseRedirect('contribution/' + str(dup.contribution.id))
                else:
                    #url contribution with comment
                    url = form.data['url']
                    url_contr = Url(url = url, contribution = contr)
                    url_contr.save()
                    text = form.data['text']
                    comm = Commentary(text = text, contribution = contr, user = UserProfile.objects.get(pk=request.user.id))
                    comm.save()
                    contr.ncomments += 1
                    contr.save()
            return HttpResponseRedirect('new')

def new(request):
    posts = Contribution.objects.order_by('-date')
    urls = Url.objects.all()
    titles = OnlyTitle.objects.all()
    contr_con_likes = Contribution_points.objects.filter(user_id=request.user.id)
    return render(request, 'aswprojecte_app/new.html', {'posts': posts, 'urls': urls, 'titles': titles, 'cont_likes':contr_con_likes})


def contribution_detail(request, pk):
    likeado = False;
    post = get_object_or_404(Contribution, pk=pk)
    urls = Url.objects.all()
    asks = Ask.objects.all()
    comm_con_likes = Commentary_points.objects.filter(user_id=request.user.id)
    comm_p=""
    form = CommentForm()
    if(UserProfile.objects.filter(pk=request.user.id).exists()):
        user = UserProfile.objects.get(pk=request.user.id)
        asdf = Contribution_points.objects.filter(user = user, contribution = post)
        if asdf.exists():
            likeado = True
        if(Commentary_points.objects.filter(user = user).exists()):
            comm_p = Commentary_points.objects.all()
    comments = post.comments.filter(father_comm__isnull=True)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    return render(request, 'aswprojecte_app/contribution_detail.html', {'post': post, 'urls_or_ask': urls_or_ask, 'comments': comments, 'likeado':likeado, 'comm_p':comm_p, 'urls':urls, 'asks':asks, 'comm_likes':comm_con_likes, 'form':form})


def add_comment_to_post(request, pk):
    post = get_object_or_404(Contribution, id=pk)
    comments = post.comments.filter(father_comm__isnull=True)
    urls = Url.objects.all()
    asks = Ask.objects.all()
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            user = UserProfile.objects.get(pk=request.user.id)
            comment.user = user
            comment.contribution = post
            comment.father_comm__isnull=False
            comment.save()
            user.karma += 1
            user.save()
            c = Contribution.objects.get(pk=pk)
            c.ncomments= c.ncomments + 1
            c.save()
            form = CommentForm()
            return render(request, 'aswprojecte_app/contribution_detail.html', {'post': post, 'urls_or_ask': urls_or_ask, 'comments': comments, 'urls':urls, 'asks':asks, 'form':form})
    else:
        form = CommentForm()
        return render(request, 'aswprojecte_app/contribution_detail.html', {'post': post, 'urls_or_ask': urls_or_ask, 'comments': comments, 'urls':urls, 'asks':asks,'form':form})

def like_post(request, pk):
    post = get_object_or_404(Contribution, id = pk)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    post.likes.add(request.user.id)
    user = UserProfile.objects.get(pk=request.user.id)
    existes = Contribution_points.objects.filter(user=user, contribution=post)
    if not existes.exists():
        new_contribution_point = Contribution_points(user = user, contribution = post)
        new_contribution_point.save()
    return HttpResponseRedirect(reverse('post_detail', args=[str(pk)]))


def unlike_post(request, pk):
    post = get_object_or_404(Contribution, id = pk)
    user = UserProfile.objects.get(pk=request.user.id)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    post.likes.remove(request.user.id)
    asdf2 = Contribution_points.objects.get(user = user, contribution = post)
    asdf2.delete()
    return HttpResponseRedirect(reverse('post_detail', args=[str(pk)]))

def like_comment(request, pk, pk_comment):
    post = get_object_or_404(Contribution, id = pk)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    comments = post.comments.filter(father_comm__isnull=True)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    #user = UserProfile.objects.get(pk=request.user.id)
    comm = Commentary.objects.get(pk=pk_comment)
    comm.likes.add(request.user.id)
    user = UserProfile.objects.get(pk=request.user.id)
    existes = Commentary_points.objects.filter(user=user, commentary=comm)
    if not existes.exists():
        new_comm_point = Commentary_points(user = user, commentary = comm)
        new_comm_point.save()
    return HttpResponseRedirect(reverse('post_detail', args=[str(pk)]))


def unlike_comment(request, pk, pk_comment):
    post = get_object_or_404(Contribution, id = pk)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    comments = post.comments.filter(father_comm__isnull=True)
    comm_p = Commentary_points.objects.all()
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    #user = UserProfile.objects.get(pk=request.user.id)
    comm = Commentary.objects.get(pk=pk_comment)
    comm.likes.remove(request.user.id)
    user = UserProfile.objects.get(pk=request.user.id)
    existes = Commentary_points.objects.filter(user=user, commentary=comm)
    if existes.exists():
        asdf2 = Commentary_points.objects.get(user = user, commentary = comm)
        asdf2.delete()
    return HttpResponseRedirect(reverse('post_detail', args=[str(pk)]))

def reply_comment(request, pk, pk_comment):
    post = get_object_or_404(Contribution, id=pk)
    comment_parent = get_object_or_404(Commentary, id=pk_comment)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    comments = post.comments.filter(father_comm__isnull=True)
    urls = Url.objects.all()
    asks = Ask.objects.all()
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            reply = form.save(commit=False)
            user = UserProfile.objects.get(pk=request.user.id)
            user.karma += 1
            user.save()
            reply.user = user
            reply.contribution = post
            reply.father_comm = comment_parent
            reply.father_comm__isnull=False
            reply.save()
            post.ncomments= post.ncomments + 1
            post.save()
            form = CommentForm()
            return render(request, 'aswprojecte_app/contribution_detail.html', {'post': post, 'urls_or_ask': urls_or_ask, 'comments':comments, 'urls': urls, 'asks':asks, 'form':form})
    else:
        form = CommentForm()
        return render(request, 'aswprojecte_app/reply_comment.html', {'form': form})


def like_post_home(request, pk):
    post = get_object_or_404(Contribution, id = pk)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    post.likes.add(request.user.id)
    user = UserProfile.objects.get(pk=request.user.id)
    existes = Contribution_points.objects.filter(user=user, contribution=post)
    if not existes.exists():
        new_contribution_point = Contribution_points(user = user, contribution = post)
        new_contribution_point.save()
    return HttpResponseRedirect(reverse('Home'))


def unlike_post_home(request, pk):
    post = get_object_or_404(Contribution, id = pk)
    user = UserProfile.objects.get(pk=request.user.id)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    post.likes.remove(request.user.id)
    existes = Contribution_points.objects.filter(user=user, contribution=post)
    if existes.exists():
        asdf2 = Contribution_points.objects.get(user = user, contribution = post)
        asdf2.delete()
    return HttpResponseRedirect(reverse('Home'))


def like_post_new(request, pk):
    post = get_object_or_404(Contribution, id = pk)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    post.likes.add(request.user.id)
    user = UserProfile.objects.get(pk=request.user.id)
    existes = Contribution_points.objects.filter(user=user, contribution=post)
    if not existes.exists():
        new_contribution_point = Contribution_points(user = user, contribution = post)
        new_contribution_point.save()
    return HttpResponseRedirect(reverse('New'))


def unlike_post_new(request, pk):
    post = get_object_or_404(Contribution, id = pk)
    user = UserProfile.objects.get(pk=request.user.id)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    post.likes.remove(request.user.id)
    existes = Contribution_points.objects.filter(user=user, contribution=post)
    if existes.exists():
        asdf2 = Contribution_points.objects.get(user = user, contribution = post)
        asdf2.delete()
    return HttpResponseRedirect(reverse('New'))



def like_post_ask(request, pk):
    post = get_object_or_404(Contribution, id = pk)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    post.likes.add(request.user.id)
    user = UserProfile.objects.get(pk=request.user.id)
    existes = Contribution_points.objects.filter(user=user, contribution=post)
    if not existes.exists():
        new_contribution_point = Contribution_points(user = user, contribution = post)
        new_contribution_point.save()
    return HttpResponseRedirect(reverse('Ask'))


def unlike_post_ask(request, pk):
    post = get_object_or_404(Contribution, id = pk)
    user = UserProfile.objects.get(pk=request.user.id)
    urls_or_ask = Url.objects.filter(contribution_id=pk)
    if urls_or_ask.exists():
        urls_or_ask = Url.objects.get(contribution_id=pk)
    else:
        urls_or_ask = Ask.objects.filter(contribution_id=pk)
        if urls_or_ask.exists():
            urls_or_ask = Ask.objects.get(contribution_id=pk)
        else:
            urls_or_ask=""
    post.likes.remove(request.user.id)
    existes = Contribution_points.objects.filter(user=user, contribution=post)
    if existes.exists():
        asdf2 = Contribution_points.objects.get(user = user, contribution = post)
        asdf2.delete()
    return HttpResponseRedirect(reverse('Ask'))



def like_comment_threads(request, pk, pk_comment):
    comm = Commentary.objects.get(pk=pk_comment)
    comm.likes.add(request.user.id)
    user = UserProfile.objects.get(pk=request.user.id)
    existes = Commentary_points.objects.filter(user=user, commentary=comm)
    if not existes.exists():
        new_comm_point = Commentary_points(user = user, commentary = comm)
        new_comm_point.save()

    usr = UserProfile.objects.get(pk=request.user.id)
    comm_con_likes = Commentary_points.objects.filter(user_id=request.user.id)
    comments = Commentary.objects.filter(user = usr).exclude(father_comm__user=usr) & Commentary.objects.filter(user = usr).exclude(father_comm__father_comm__user=usr)
    return render(request, 'aswprojecte_app/threads.html', {'comments': comments, 'user_mio':usr, 'comm_likes':comm_con_likes})


def unlike_comment_threads(request, pk, pk_comment):
    comm = Commentary.objects.get(pk=pk_comment)
    comm.likes.remove(request.user.id)
    user = UserProfile.objects.get(pk=request.user.id)
    existes = Commentary_points.objects.filter(user=user, commentary=comm)
    if existes.exists():
        asdf2 = Commentary_points.objects.get(user=user, commentary=comm)
        asdf2.delete()

    usr = UserProfile.objects.get(pk=request.user.id)
    comm_con_likes = Commentary_points.objects.filter(user_id=request.user.id)
    comments = Commentary.objects.filter(user = usr).exclude(father_comm__user=usr) & Commentary.objects.filter(user = usr).exclude(father_comm__father_comm__user=usr)
    return render(request, 'aswprojecte_app/threads.html', {'comments': comments, 'user_mio':usr, 'comm_likes':comm_con_likes})
